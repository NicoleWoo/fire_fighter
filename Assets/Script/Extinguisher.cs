﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Extinguisher : MonoBehaviour {

	public int strength=1; 

	public ParticleSystem psExtinguisher;
	List <ParticleCollisionEvent> collisionEvent; 

	void Start() {
		collisionEvent = new List<ParticleCollisionEvent> ();
	}

	void OnParticleCollision(GameObject other) {

		//Getting the list of particle collision event
		int numberEvent = psExtinguisher.GetCollisionEvents(other, collisionEvent);
		
		//Fire fire = other.GetComponent<Fire> ();
		//fire.TakeDamage (10);

		/*for (int i = 0; i < numberevent; i++) {
			
			if (collisionevent[i].collidercomponent.comparetag ("fire")) {
				collisionevent[i].collidercomponent.getcomponent<fire> ().takedamage (1);
				debug.log("killing fire");
			}

		}*/

		if (other.CompareTag ("Fire")) {
			other.GetComponent<Fire> ().TakeDamage (strength);
			Debug.Log("killing fire");
		}



	}

}
