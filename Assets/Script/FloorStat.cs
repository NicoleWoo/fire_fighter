﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorStat : Stat {

	const int MAX_HEALTH = 100;
	const float FIRE_SPREAD_CHECK_TIME = 5f;

	//public int heat; // heat stats will determine if fire is spawn
	public bool isOnFire = false;
	public GameObject fire;
	public bool isFlameable = false; // by default is false

	private GameObject obj;
	private Fire fireScript; 
	private bool isBurning;
	private bool isSpreadingFire;
	private List<Vector2> nearbyTilesDirection;
	private bool recentExtinguished; 

    void Start() {
		health = MAX_HEALTH; 
        GetObjectNearby(Vector2.left);
        isFlameable = true;
		isSpreadingFire = false;
		recentExtinguished = false;
		nearbyTilesDirection = new List<Vector2>(); 
		nearbyTilesDirection.Add (Vector2.left);
		nearbyTilesDirection.Add (Vector2.right);
		nearbyTilesDirection.Add (Vector2.up);
		nearbyTilesDirection.Add (Vector2.down);
    }

	void Update() {
		if (isOnFire && (fireScript.health >=  Fire.MAX_HEALTH)) { //Once fire reach full size 
			
			if (health > 0) { 									 //Check floor is ready to spread fire
				if (!isBurning) {
					isBurning = true;								 // Decrease floor health till it is ready to spread fire
					StartCoroutine (Burning (10));
					Debug.Log("Decrease Floor HP");
				}
			} else if(!isSpreadingFire) {
				isSpreadingFire = true; 
				StartCoroutine(SpreadFire ());
			}
		} 
		/*else if (isOnFire && fireScript.health < 100 && obj.activeSelf == true) {		// if fire is not full size
			
			if (health < 100) {																			// slowly increase floor health to max
				Debug.Log("increase Floor HP");
				isBurning = true;								 											
				StartCoroutine (Burning (-1));
			}

		}*/
		else if (isOnFire && obj.activeSelf == false) {		// If fire is putted out
			FireOut (); 										// set state back to normal
		}

        Debug.DrawRay(transform.position, Vector2.left, Color.green);        
    }

    private void SetOnFire()
    {
		if (!recentExtinguished) {							// CHeck if not recent extinguised
			obj = (GameObject)Instantiate (fire);
			obj.transform.position = transform.position;
			obj.SetActive (true);
			isOnFire = true;
			fireScript = obj.GetComponent<Fire> ();
		} else {
			recentExtinguished = false;
		}
    }

    public void FireOut()
    {
        Debug.Log("fire out");
        isOnFire = false;
		SetHealth (MAX_HEALTH);							// reset health so it won't spread so fast right after
        Destroy(obj);
		recentExtinguished = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
	{		
		if (collision.CompareTag("Igniter"))
		{
			Debug.Log("Ignite fire");
            Debug.Log("isOnFire = " + isOnFire);
            if (!isOnFire)
            {
                SetOnFire();
            }
            
        }
	}

	// Reduced TakeDamage to avoid going into onDeath()
	public override void TakeDamage (int damage)
	{
		health -= damage;
	}

	// Burn the Floor
	IEnumerator Burning(int damage)
	{
        TakeDamage(damage);
		yield return new WaitForSeconds(1f);
		isBurning = false;
	}		

	IEnumerator SpreadFire (){
		Debug.Log("Spreading Fire");
		foreach (Vector2 direction in nearbyTilesDirection) {	// Loop through all direction

			RaycastHit2D hit = GetObjectNearby(direction);		//Spreading fire Left side
			if (hit == false) {
				Debug.Log ("NULL Raycast");
			} else if (hit.transform.CompareTag ("Floor")) {
				Debug.Log ("Spreading");
				Debug.Log ("Raycast " + hit.collider.name);
				FloorStat fs = hit.transform.GetComponent<FloorStat> (); //get the object nearby's script
				if (!fs.isOnFire) {						// Check if it's on Fire already
					fs.SetOnFire ();						// Set it on fire if it is not
				}
			} else if (hit.transform.CompareTag ("Building")) {
				Debug.Log ("Raycast " + hit.collider.name);
				hit.transform.GetComponent<Stat> ().TakeDamage (10);
			}
		}

		yield return new WaitForSeconds(FIRE_SPREAD_CHECK_TIME);		// Wait before checking again
		isSpreadingFire = false;


	}

	// This will use invisiable ray to find object nearby as per direction passed in
	RaycastHit2D GetObjectNearby(Vector2 direction)
	{
		RaycastHit2D hit = Physics2D.Raycast(transform.position, direction,Mathf.Infinity, LayerMask.GetMask("Building", "Floor"));
		if (hit == true)
		{
			Debug.Log("Raycast " + hit.collider.name);
		}
		else
		{
			Debug.Log("Raycast no hit");
		}

		return hit; 
	}
}
