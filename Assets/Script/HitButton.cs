﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class HitButton : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    public bool isPressed = false;

    public static bool wall;
    public static bool door;
    public static bool person;

    public Sprite hitWallButton;
    public Sprite openDoorButton;
    public Sprite pickUpPerson;
    public Sprite defaultButton;

    private Image button;

    private void Start()
    {
        button = GetComponent<Image>();
    }

    private void Update()
    {
        if (wall)
        {
            button.sprite = hitWallButton;
        }
        else if (door)
        {
            button.sprite = openDoorButton;
        }
        else if (person)
        {
            button.sprite = pickUpPerson;
        }
        else
        {
            button.sprite = defaultButton;
        }
    }

    public virtual void OnPointerDown(PointerEventData ped)
    {
        isPressed = true;
    }

    public virtual void OnPointerUp(PointerEventData ped)
    {
        isPressed = false;
    }
}
