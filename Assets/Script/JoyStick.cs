﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class JoyStick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    private Image background, joystick;
    private Vector3 inputVector;

    private void Start()
    {
        background = GetComponent<Image>();
        joystick = transform.GetChild(0).GetComponent<Image>();
    }

    public virtual void OnDrag(PointerEventData ped)
    {
        Vector2 position;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(background.rectTransform, ped.position, ped.pressEventCamera, out position))
        {
            position.x = position.x / background.rectTransform.sizeDelta.x;
            position.y = position.y / background.rectTransform.sizeDelta.y;

            inputVector = new Vector3((position.x * 2) - 1, 0, (position.y * 2) - 1);
            
            if(inputVector.magnitude > 1.0f)
            {
                inputVector = inputVector.normalized;
            }

            // Move Joystick
            joystick.rectTransform.anchoredPosition = new Vector3(inputVector.x * (background.rectTransform.sizeDelta.x / 3),
                                                                    inputVector.z * (background.rectTransform.sizeDelta.y / 3));
        }
    }

    public virtual void OnPointerDown(PointerEventData ped)
    {
        /*Debug.Log(ped.position);
        if (ped.position.x > 100 && ped.position.y > 100)
        {
            background.rectTransform.anchoredPosition = new Vector3(ped.position.x, 0, ped.position.y);
        }*/
        OnDrag(ped);
    }

    public virtual void OnPointerUp(PointerEventData ped)
    {
        inputVector = Vector3.zero;
        joystick.rectTransform.anchoredPosition = Vector3.zero;
    }

    public float Horizontal()
    {
        if(inputVector.x != 0)
        {
            return inputVector.x;
        }
        else
        {
            return Input.GetAxis("Horizontal");
        }
    }

    public float Vertical()
    {
        if (inputVector.z != 0)
        {
            return inputVector.z;
        }
        else
        {
            return Input.GetAxis("Vertical");
        }
    }
}
