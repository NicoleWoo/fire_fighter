﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectStat : Stat {

	private bool isBurning = false;
	private bool isTrigger = false;

	void Start(){
		//Objects will have 1000 hp instead
		//health = 1000; let user chose health instead
	}

	void Update(){
		// if coroutine buring is note running yet
		if (isTrigger && !isBurning)
		{
            isBurning = true;
            StartCoroutine(Burning(100));			
		}
	}



	private void OnTriggerEnter2D(Collider2D collision)
	{		
		if (collision.CompareTag("Fire"))
		{
			Debug.Log(""+ base.name+ " Fire trigger");
			isTrigger = true;
		}

	}

	private void OnTriggerExit2D(Collider2D collision)
	{
		//Debug.Log("Object exit trigger");
		isTrigger = false;
	}

	IEnumerator Burning(int damage)
	{
		TakeDamage(damage);
		yield return new WaitForSeconds(0.5f);
		isBurning = false;
		Debug.Log("Object burning");
	}		

}
