﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAction : HitButton
{
    private void Start()
    {
        wall = false;
        door = false;
        person = false;
    }

    void Update ()
    {
		if (isPressed)
        {

        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Building"))
        {
            wall = true;
        }

        if (collision.CompareTag("Door"))
        {
            door = true;
        }

        if (collision.CompareTag("Person"))
        {
            person = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Building"))
        {
            wall = false;
        }

        if (collision.CompareTag("Door"))
        {
            door = false;
        }

        if (collision.CompareTag("Person"))
        {
            person = false;
        }
    }
}
