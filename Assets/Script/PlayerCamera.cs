﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour {

	public float dampTime = 0.2f;
	public Transform target;


	private Camera playerCamera;
	private Vector2 moveVelocity;
	private Vector2 desiredPosition;

	private void Awake() {
		//playerCamera = GetComponentInChildren<Camera> ();
	}

	private void FixedUpdate() {
		
		Move ();
		transform.position = Vector2.SmoothDamp (transform.position, desiredPosition, ref moveVelocity, dampTime,Mathf.Infinity, Time.deltaTime);

	}

	private void Move() {

		desiredPosition = target.position;

	}

}
