﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public float moveSpeed;
    public float turnSpeed;
	//public Vector2 velocity;

	private Rigidbody2D rb2d;
	private float moveHorizontal;
	private float moveVeritical;

	// Use this for initialization
	void Start () {
		rb2d = GetComponent<Rigidbody2D>();
		moveHorizontal = 0;
		//rb2d.isKinematic = true;
	}
	
	// run once per frame
	void Update(){
		//Getting user input
		moveHorizontal = Input.GetAxis ("Horizontal");
		moveVeritical = Input.GetAxis ("Vertical");
	}

	//run every physic frame
	void FixedUpdate () {

		Move ();
	}

	//function to move the player
	private void Move(){
		Vector2 movement = new Vector2 (moveHorizontal, moveVeritical);
        rb2d.velocity = movement * moveSpeed;
        //rb2d.AddForce (movement * speed);

        //Flipping object not needed for top down view

        //Vector2 moveH = transform.forward*moveHorizontal*speed*Time.fixedDeltaTime;
        //rb2d.MovePosition (rb2d.position + moveH);

        //Rotation
        //only change if there's some input
        if(moveHorizontal!=0 || moveVeritical != 0)
        {
            float targetAngle = Mathf.Atan2(moveVeritical, moveHorizontal) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, targetAngle), turnSpeed * Time.fixedDeltaTime);
        }
        

        //rb2d.rotation = Quaternion.Euler();

	}
}
