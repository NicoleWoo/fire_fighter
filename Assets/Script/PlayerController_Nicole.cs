﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController_Nicole : MonoBehaviour {

    public float moveSpeed;
    public float turnSpeed;
    //public Vector2 velocity;

    public JoyStick joystick;
    public WaterButton button;

    private Rigidbody2D rb2d;
    private float moveHorizontal;
    private float moveVeritical;
	private float bExtinguish;
    private Animator anim;
	private ParticleSystem psExtinguisher;
    private CapsuleCollider2D collider2;
    private bool isEmitting = false;
    

    /*[SerializeField]
    private PolygonCollider2D[] colliders;
    private PolygonCollider2D currentCollider;
    private int currentColliderIndex=0;*/
    

    // Use this for initialization
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        moveHorizontal = 0;
        anim = GetComponent<Animator>();
		psExtinguisher = GetComponentInChildren<ParticleSystem> ();
        //colliders = GetComponents<PolygonCollider2D>();
        //collide2d = GetComponent<Collider2D>();
        //rb2d.isKinematic = true;
    }

    // run once per frame
    void Update()
    {
        //Getting user input
        moveHorizontal = joystick.Horizontal();
        moveVeritical = joystick.Vertical();


        if (moveHorizontal != 0 || moveVeritical != 0)
        {
            //update animation
            anim.SetBool("PlayerMove", true);
        }
        else
        {
            anim.SetBool("PlayerMove", false);
        }

		if (button.isPressed) //if button pressed
        {
            if (!isEmitting && !button.isEmpty)
            {
                StartCoroutine(EmissionRate(0.1f));
            }
		}
    }

    //run every physic frame
    void FixedUpdate()
    {
        Move();
    }

    //function to move the player
    private void Move()
    {
        Vector2 movement = new Vector2(moveHorizontal, moveVeritical);
        rb2d.velocity = movement * moveSpeed;
        //rb2d.AddForce (movement * speed);

        //Rotation
        //only change rotation if there's some input
        if (moveHorizontal != 0 || moveVeritical != 0)
        {
            float targetAngle = Mathf.Atan2(moveVeritical, moveHorizontal) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, targetAngle), turnSpeed * Time.fixedDeltaTime);
            
        }

    }

    // this is for updating sprite in animation event //Only needed if use multiple collider for sprite
    public void SetColliderForSprite(int spriteNum)
    {
        //colliders[currentColliderIndex].enabled = false;
        //currentColliderIndex = spriteNum;
        //colliders[currentColliderIndex].enabled = true;
   
    }

    IEnumerator EmissionRate(float delay)
    {
        isEmitting = true;
        psExtinguisher.Emit(1);
        yield return new WaitForSeconds(delay);
        isEmitting = false;
    }

}
