﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStat : Stat {

	private bool isBurning = false;

	void Start(){
		//Objects will have 1000 hp instead
		//health = 100; 
	}

	private void OnTriggerStay2D(Collider2D collision)
	{
		//Debug.Log("Object stay trigger");

		//if player and not burning
		if (collision.CompareTag("Fire"))
		{
			// if coroutine burning is not running yet
			if (!isBurning)
			{
                isBurning = true;
                StartCoroutine(Burning(1));                
            }
		}
	}

	IEnumerator Burning(int damage)
	{
		TakeDamage(damage);
		yield return new WaitForSeconds(0.5f);
		isBurning = false;
		Debug.Log("burning");
	}
}
