﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stat : MonoBehaviour {

	// PARENT CLASS: Not to be used directly

	public int health;
	public bool isAlive = true;




	public virtual void TakeDamage (int damage){
		
		if (health <= 0 && isAlive) {
			isAlive = false;
			OnDeath ();
		} else {
			health -= damage;
		}
	}

	public void SetHealth (int hp){
		health = hp; 
	}

	public void SetAlive (bool alive){
		//check if state is already there
		if (isAlive != alive) {
			//set to new state
			isAlive = alive;
			//update the animator
			if (isAlive) {
				OnResurrected();
			} else {
				OnDeath();
			}
		}

	}

	protected virtual void OnDeath (){
		//child implement
		gameObject.SetActive(false);
	}

	protected virtual void OnResurrected(){
		//child implement
		gameObject.SetActive (true);
	}
		

}
