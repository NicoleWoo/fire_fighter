﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallInteractions : MonoBehaviour
{
    public GameObject fireAxe;

	// Use this for initialization
	void Start ()
    {
        fireAxe.SetActive(false);
    }
	
	void OnTriggerEnter2D (Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            fireAxe.SetActive(true);
        }
	}

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            fireAxe.SetActive(false);
        }
    }
}
