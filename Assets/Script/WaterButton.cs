﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class WaterButton : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    public float speed = 0.001f;
    public Image water;
    public bool isPressed = false;
    public bool isEmpty = false;

    private void Update()
    {
        if(isPressed)
        {
            water.fillAmount -= speed;
        }

        if(water.fillAmount == 0)
        {
            isEmpty = true;
        }
    }

    public virtual void OnPointerDown(PointerEventData ped)
    {
        isPressed = true;
    }

    public virtual void OnPointerUp(PointerEventData ped)
    {
        isPressed = false;
    }
}
