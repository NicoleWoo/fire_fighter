﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : Stat {

    //bool isTrigger=false;
    //bool isBurning = false;
	bool isGrowing = false; 
	public const int MAX_HEALTH = 100; 
	private float minSize = 0.05f;
	private float maxSize = 0.2f;
	//private float maxHealth = 100; 
	private Collider2D c2d; 

	private void Start() {
		c2d = GetComponent<Collider2D> (); 
		health = 1;
		FireSize ();
		GetObjectNearby ();
	}

    private void Update()
    {
		if (!isGrowing) {
			StartCoroutine( FireGrow (5));
		}
		//GetObjectNearby ();
    }

	public override void TakeDamage (int damage)
	{
		base.TakeDamage (damage);

	}

	private void FireSize(){
		float actualScale = (health / (float)MAX_HEALTH) * (maxSize);
		if (actualScale < minSize && health > 0) {
			actualScale = minSize;
		}

		transform.localScale = new Vector2 (actualScale, actualScale);
	}


   
   /* private void OnTriggerStay2D(Collider2D collision)
    {
        //Debug.Log("Object stay trigger");

        //if player and not burning
        if (collision.attachedRigidbody.CompareTag("Player"))
        {
            // if coroutine buring is note running yet
            if (!isBurning)
            {
                StartCoroutine(Burning(collision, 1));
                isBurning = true;
            }
        }
		else if(collision.attachedRigidbody.CompareTag(""))
		{
			if (!isBurning)
			{
				StartCoroutine(Burning(collision, 1));
				isBurning = true;
			}
		}

    }*/


	IEnumerator FireGrow(int strength)
	{		
		isGrowing = true; 
		if (health < MAX_HEALTH) {
			health += strength;
		}
		FireSize ();
		yield return new WaitForSeconds(0.5f);
		isGrowing = false;
	}

	void GetObjectNearby () { 
		
		/*List<RaycastHit2D> myResult;
		Vector2 direction = new Vector2 (0, 1);
		ContactFilter2D cf2d = new ContactFilter2D ();
		cf2d.layerMask = LayerMask.GetMask ("House");

		Debug.DrawRay (transform.position, Vector2.right,Color.green);

		c2d.Raycast (Vector2.right, cf2d,myResult);

		Debug.Log ("Length " + myResult.Count);

		if (myResult.Length != null) {
			for(int i = 0; i<myResult.Length; i++){
				Debug.Log ("Raycast " + myResult [i].transform.name);
			}
		} else {
			Debug.Log ("NULL Array");
		}*/

		//*******************************************
		/*Vector2 direction = Vector2.right;
		RaycastHit2D hit = Physics2D.Raycast(transform.position, direction,Mathf.Infinity, LayerMask.GetMask("House"));
		Debug.Log ("Raycast " + hit.collider.name);*/
	}
    
}
